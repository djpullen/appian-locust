*********************************************************************
Appian Locust Library documentation
*********************************************************************

.. sidebar:: About Appian-Locust

    * **appian-locust** is a tool for load and performance testing Appian. It is a wrapper around the `Locust <https://docs.locust.io/en/stable/>`__ load driving framework to enable driving load against Appian instance.
    * `View/contribute on gitlab <https://gitlab.com/appian-oss/appian-locust>`__

.. toctree::
    :maxdepth: 1
    :caption: Getting Started

    what_is_appian_locust
    quick_start
    how_to_write_locust_tests

.. toctree::
    :maxdepth: 1
    :caption: Record Appian Locust Tests

    appian_locust_companion

.. toctree::
    :maxdepth: 1
    :caption: How to Run Tests

    how_to_run_locust
    debugging
    useragent
    limitations

.. toctree::
    :maxdepth: 1
    :caption: Advanced Concepts

    advanced_appian_locust_usage
    advanced_tests

.. toctree::
    :maxdepth: 1
    :caption: Development References

    release_notes
    migration_guides/v2_upgrade
    api
    contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
